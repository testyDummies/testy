<?php
/**
 * Category model config
 */
return array(
    'title' => 'Categories',
    'single' => 'category',
    'model' => 'Testy\Models\Category',
    /**
     * The display columns
     */
    'columns' => array(
        'id',
        'category_name' => array(
            'title' => 'Name',
            'select' => "category_name",
        ),
        'category_tests' => array(
            'title' => '# films',
            'select' => 'category_tests',
        ),
    ),
    /**
     * The filter set
     */
    'filters' => array(
        'id',
        'category_name' => array(
            'title' => 'Category',
        ),
        'category_tests' => array(
            'title' => 'Number of tests in it',
        ),
    ),
    /**
     * The editable fields
     */
    'edit_fields' => array(
        'category_name' => array(
            'title' => 'Category',
            'type' => 'text',
        ),
    ),
);