<?php

namespace Testy\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Testy\Models\User;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Dertermine if the given user can invite users
     *
     * @param User $user
     * @return bool
     * @internal param $password
     * @internal param User $user
     * @internal param Group $group
     */
    public function userIdentity(User $user) { 
        return Hash::check(Input::get('password_name'), $user->password);
    }
}
