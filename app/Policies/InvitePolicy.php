<?php

namespace Testy\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Testy\Models\Group;
use Testy\Models\User;

class InvitePolicy
{
    use HandlesAuthorization;

    /**
     * Dertermine if the given user can invite users
     *
     * @param User $user
     * @param Group $group
     * @return bool
     */
    public function inviteUsers(User $user, Group $group) {
        return $user->id == $group->group_owner_id;
    }
}
