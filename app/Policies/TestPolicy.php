<?php

namespace Testy\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Testy\Models\Test;
use Testy\Models\User;

class TestPolicy
{
    use HandlesAuthorization;

    /**
     * Dertermine if the given user can modife data
     * of given test
     *
     * @param User $user
     * @param Test $test
     * @return bool
     */
    public function editData(User $user, Test $test) {
        return $user->id == $test->user_id;
    }
}
