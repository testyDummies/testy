<?php
/**
 * Created by PhpStorm.
 * User: vi4ov
 * Date: 4.2.2017 г.
 * Time: 22:28
 */

namespace Testy\Services;


use Illuminate\Http\Request;
use Testy\Models\Category;

class CategoryService extends BaseService
{
    public function __construct(Category $model)
    {
        parent::__construct($model);
    }

    /**
     * Handling new categories if there is any.
     *
     * @param Request $request
     * @return array
     */
    public function handlingNewCategories(Request $request)
    {
        $categories = Category::all('id');
        $categoriesToSave = $request->categories;
        $categoriesToSync = [];

        foreach ($categories as $category) {
            foreach ($categoriesToSave as $k => $categoryToSave) {
                if ($category->id == $categoryToSave) {
                    array_push($categoriesToSync, $categoriesToSave[$k]);
                    unset($categoriesToSave[$k]);
                }
            }
        }
        $newCategories = [];
        if(count($categoriesToSave)){
            $newCategories = $this->savingNewCategories($categoriesToSave);
        }

        $categoriesToSync = array_merge($categoriesToSync, $newCategories);
        return $categoriesToSync;
    }
    /**
     *  Saving new categories.
     *
     * @param $categoriesToSave
     * @return array
     */
    private function savingNewCategories($categoriesToSave)
    {
        $newCategories = [];
        foreach ($categoriesToSave as $categoryToSave) {
            $category = Category::create([
                'category_name' => $categoryToSave,
                'category_tests' => 0
            ])->getAttribute('id');

            array_push($newCategories, $category);
        }
        return $newCategories;
    }
}