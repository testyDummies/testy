<?php

namespace Testy\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class BaseService
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function create(Request $request)
    {
        $model = $this->model->create($request->all());
        return $model;
    }

    public function update(Request $request, $model)
    {
        $model->update($request->all());
        return $model;
    }

    public function remove(Model $model)
    {
        $model->delete();
    }
}