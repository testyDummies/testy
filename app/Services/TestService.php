<?php

namespace Testy\Services;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Testy\Infrastructure\FileHandler;
use Testy\Models\Test;

class TestService extends BaseService
{
    private $categoryService;
    private $tagService;
    private $fileHandler;

    public function __construct(CategoryService $categoryService, TagService $tagService, FileHandler $fileHandler)
    {
        $this->categoryService = $categoryService;
        $this->tagService = $tagService;
        $this->fileHandler = $fileHandler;

        parent::__construct(new Test());
    }

    public function getIndexTests(Request $request){
        $tests = $this->model->orderBy('created_at', 'desc')->get();

        $sortRequest = $request->sort;

        if($sortRequest != 'none') {
            if($sortRequest == 'popular') {
                $tests = $tests->sortBy(function($test)
                {
                    return $test->results->count();
                }, SORT_REGULAR, true);
            }

            if($sortRequest == 'biggest') {
                $tests = $tests->sortBy(function($test)
                {
                    return $test->questions->count();
                }, SORT_REGULAR, true);
            }

            if($sortRequest == 'hardest') {
                $tests = Test::select(DB::raw('tests.*, count(*) as `aggregate`'))
                    ->join('results', 'results.test_id', '=', 'tests.id')
                    ->groupBy('test_id')
                    ->orderBy('results.result_percent')
                    ->get();
            }
        }

        $tests = $tests->take(12);

        return $tests;
    }

    public function create(Request $request)
    {
        $receivedCategories = $this->categoryService->handlingNewCategories($request);
        $receivedTags = $this->tagService->handlingNewTags($request);
        $imagePath = $this->fileHandler->saveFile($request->test_image, 'tests_covers');

        $test = $this->model->create([
            'test_name'                 => $request->test_name,
            'test_short_description'    => $request->test_short_description,
            'test_description'          => $request->test_description,
            'test_img_url'              => $imagePath,
            'user_id'                   => Auth::user()->id,
            'language_id'               => $request->language_id,
            'active'                    => 0,
            'test_creation_stage'       => 2
        ]);

        $test->categories()->sync($receivedCategories);
        $test->tags()->sync($receivedTags);

        return $test;
    }

    public function update(Request $request, $test)
    {
        $recievedCategories = $this->categoryService->handlingNewCategories($request);
        $recievedTags = $this->tagService->handlingNewTags($request);


        $test->update([
            'test_name'                 => $request->test_name,
            'test_short_description'    => $request->test_short_description,
            'test_description'          => $request->test_description,
            'user_id'                   => Auth::user()->id,
            'language_id'               => $request->language_id,
            'active'                    => 0,
            'test_creation_stage'       => 4
        ]);

        $test->categories()->sync($recievedCategories);
        $test->tags()->sync($recievedTags);
    }

    public function createTestQuestionDetails(Request $request, Test $test) {
        $shuffleQuestions = false;

        if ($request->test_shuffle_questions) {
            $shuffleQuestions = $request->test_shuffle_questions;
        }

        if ($test->test_creation_stage != 2) return false;

        $test->update([
            'test_displayed_questions'  => $request->test_displayed_questions,
            'test_shuffle_questions'    => $shuffleQuestions,
            'test_creation_stage'       => 3
        ]);

        return true;
    }

    public function createTestQuestions(Request $request, Test $test) {
        if ($test->test_creation_stage != 3) return false;

        $test->update([
            'test_creation_stage'       => 4
        ]);

        return true;
    }

    public function addImage(Request $request, Test $test) {
        if (!$test->test_img_url) {
            $imageName = $this->fileHandler->saveFile($request->test_image, 'tests_covers');
            $test->update([
                'test_img_url' => $imageName
            ]);
        }

        return true;
    }

    public function removeImage(Test $test) {
        if ($test->test_img_url) {
            $this->fileHandler->deleteFile($test->test_img_url, 'tests_covers');
            $test->update([
                'test_img_url' => null
            ]);
        }
        return true;
    }
}