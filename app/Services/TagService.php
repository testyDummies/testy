<?php

namespace Testy\Services;


use Illuminate\Http\Request;
use Testy\Models\Tag;

class TagService extends BaseService
{
    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }

    /**
     * Handling new tags if there is any.
     *
     * @param Request $request
     * @return array
     */
    public function handlingNewTags(Request $request)
    {
        $tags = Tag::all('id');
        $tagsToSave = $request->tags;
        $tagsToSync = [];
        if(!$tagsToSave) {
            return $tagsToSync;
        }
        foreach ($tags as $tag) {
            foreach ($tagsToSave as $k => $tagToSave) {
                if ($tag->id == $tagToSave) {
                    array_push($tagsToSync, $tagsToSave[$k]);
                    unset($tagsToSave[$k]);
                }
            }
        }
        $newTags = [];
        if(count($tagsToSave)){
            $newTags = $this->savingNewTags($tagsToSave);
        }

        $tagsToSync = array_merge($tagsToSync, $newTags);
        return $tagsToSync;
    }
    /**
     *  Saving new tags.
     *
     * @param $tagsToSave
     * @return array
     */
    private function savingNewTags($tagsToSave)
    {
        $newTags = [];
        foreach ($tagsToSave as $tagToSave) {
            $tag = $this->model->create([
                'name' => $tagToSave
            ])->getAttribute('id');

            array_push($newTags, $tag);
        }
        return $newTags;
    }
}