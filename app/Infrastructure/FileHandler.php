<?php
namespace Testy\Infrastructure;


use Illuminate\Support\Facades\Storage;
use phpDocumentor\Reflection\File;

class FileHandler
{
    public function deleteFile($fileName, $folderName)
    {
        $folderName = trim($folderName, '/');

        $fullPath = 'images/' . $folderName . '/' . $fileName;
        if (is_file($fullPath))
        {
            \File::delete($fullPath);
        }
    }

    /**
     * @param $file
     * @param $folderName
     * @return string
     * @internal param CreateQuestionRequest $request
     */
    public function saveFile($file, $folderName)
    {
        $fileName = '';
        if ($file) {
            $fileName = str_random(59) . '.jpeg';

            $folderName = trim($folderName, '/');

            $destination_folder = "images/" . $folderName;

            $file->move($destination_folder, $fileName);
        }

        return $fileName;
    }
}