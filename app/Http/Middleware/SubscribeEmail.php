<?php

namespace Testy\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Response;
use Testy\Http\Requests\CreateEarlySubRequest;
use Testy\Mail\NewEarlySubscriber;
use Testy\Models\EarlySubscriber;

class SubscribeEmail
{
    /**
     * The application implementation.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Create a new middleware instance.
     *
     * @param Application|\Illuminate\Contracts\Foundation\Application $app
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->app->isDownForMaintenance() && $request->is('subscriber') && $request->isMethod('post')) {
            $existingSub = EarlySubscriber::where('email', $request->email)->first();

            if ($existingSub || strlen(trim($request->name)) == 0 || !filter_var($request->email, FILTER_VALIDATE_EMAIL)) {
                return Response::make(view('errors.503')->with('error', 'It seems like you have entered invalid data, or already signed email.'));
            }

            $sub = EarlySubscriber::create([
                'name' => $request->name,
                'email' => $request->email
            ]);

            Mail::to($request->email)->queue(new NewEarlySubscriber($sub));

           return Response::make(view('errors.503')->with('success', 'Thank you, we will be in touch!'));
        }

        return $next($request);
    }
}
