<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Http\Requests\CreateGroupRequest;
use Testy\Http\Requests\CreateInviteRequest;
use Testy\Models\Group;

class GroupController extends Controller
{
    /**
     * GroupController constructor.
     *
     * JWT auth middleware
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::where('group_type',  'LIKE', '%"listable":true%')->orderBy('created_at', 'desc')->get();

        return view('group.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('group.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|CreateGroupRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateGroupRequest $request)
    {
        $chks = array('listable','invitable','publicContent');
        $types = [];
        foreach ($chks as $chk) {
            $types['group_type'][$chk] = Input::has($chk) ? true : false;
        }

        $group = Group::create([
            'group_name' => $request['group_name'],
            'group_description' => $request['group_description'],
            'group_type' => json_encode($types['group_type']),
            'group_owner_id' => Auth::user()->id
        ]);

        $group->save();

        return $this->show($group);
    }

    /**
     * Display the specified resource.
     *
     * @param Group $group
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Group $group)
    {
        $group_type = json_decode($group->group_type, true);

        return view('group.show', compact('group', 'group_type'));
    }

    /**
     * Perform join in group action for users.
     * @param Group $group
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     * @internal param Group $group
     * @internal param int $id
     */
    public function joinUser($id)
    {
        $group = Group::find($id);

        $group->users()->attach(Auth::user()->id);

        return back();
    }
}