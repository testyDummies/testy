<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Mail\ChangedPassword;
use Testy\Models\User;

class AccountController extends Controller
{
    /**
     * Instantiate new TestController instance.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Perform changing name.
     *
     * @param Request $request
     * @return array
     */
    public function changeName(Request $request) {
        $this->authorize('userIdentity', Auth::user());

        $this->validate($request, [
            'newName' => 'min:7|max:64'
        ]);

        User::where('id', Auth::user()->id)
            ->update(['name' => $request->newName]);

        return redirect()->back();
    }

    public function getModal(Request $request) {
        switch ($request->modal) {
            case 'name':
                return view('account.modals.change-name-modal');
                break;
            case 'email':
                return view('account.modals.change-email-modal');
                break;
            case 'username':
                return view('account.modals.change-username-modal');
                break;
            case 'password':
                return view('account.modals.change-password-modal');
                break;
        }
        return response(404);
    }

    /**
     * Perform changing email.
     *
     * @param Request $request
     * @return array
     */
    public function changeEmail(Request $request) {
        $this->authorize('userIdentity', Auth::user());

        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users'
        ]);

        User::where('id', Auth::user()->id)
            ->update(['email' => $request->email]);

        return redirect()->back();
    }



    /**
     * Perform changing username.
     *
     * @param Request $request
     * @return array
     */
    public function changeUsername(Request $request) {
        $this->authorize('userIdentity', Auth::user());

        $this->validate($request, [
            'username' => 'required|min:3|max:255|unique:users'
        ]);

        User::where('id', Auth::user()->id)
            ->update(['username' => $request->username]);

        return redirect()->back();
    }



    /**
     * Perform changing name.
     *
     * @param Request $request
     * @return array
     */
    public function changePassword(Request $request) {
        $this->authorize('userIdentity', Auth::user());

        $this->validate($request, [
            'password_name' => 'required',
            'password_new' => 'required|confirmed|min:6|different:password_name'
        ]);

        $user = User::where('id', Auth::user()->id);
        $user->update(['password' => bcrypt($request->password_new)]);

        Mail::to($request->user())->queue(new ChangedPassword());

        return redirect()->back();
    }
}
