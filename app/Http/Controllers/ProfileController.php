<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Models\Result;
use Testy\Models\Test;
use Testy\Models\User;

class ProfileController extends Controller
{

    /**
     * Instantiate new TestController instance.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param null $username
     * @return Response
     */
    public function index($username = null)
    {

        $tests = new Test();

        $user = null;

        $isLogged = false;
        if ($username == null)
        {
            $user = Auth::user();
            $isLogged = true;
        }
        else
        {
            $user = User::where(['username' => $username])->first();
        }

        if (!count($user)) {
            return abort(404);
        }
        $userTests = $tests->where('user_id', $user->id)->get();

        $results = User::find(Auth::user()->id)->results->each(function($result) {
            $result->test;
        });

        return view('account.index', compact('user', 'userTests', 'isLogged', 'results'));

    }
}
