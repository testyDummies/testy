<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;
use Testy\Http\Requests\CreateMultipleChoiceQuestionRequest;
use Testy\Infrastructure\FileHandler;
use Testy\Models\Question;
use Testy\Models\QuestionType;
use Testy\Models\QuestionTypes\MultipleChoiceQuestion;
use Testy\Models\QuestionTypes\MultipleChoiceQuestionAnswer;

class MultipleChoiceQuestionController extends Controller
{
    public function store(CreateMultipleChoiceQuestionRequest $request) {
        $fileHandler = new FileHandler();
        $fileName = $fileHandler->saveFile($request->img, 'multiple-choice-questions');

        $multipleChoiceQuestion = MultipleChoiceQuestion::create([
            'content' => $request['content'],
            'active' => isset($request->active) ? '1' : '0',
            'answer_visible' => isset($request->answer_visible) ? $request->answer_visible : '-1',
            'img_url' => $fileName,
            'type' => 'closed'
        ]);

        $multipleChoiceQuestion->save();

        $questionRefenrece = Question::create([
            'test_id' => $request->test_id,
            'question_type_id' => $request->type_id,
            'question_id' => $multipleChoiceQuestion->id
        ]);

        $questionRefenrece->save();

        $this->saveAnswer($request, $multipleChoiceQuestion->id);

        return response(200);
    }

    /**
     * Save answers after submitting form
     * @param $request
     * @param $question_id
     * @return boolean
     */
    private function saveAnswer($request, $question_id)
    {
        $i = 1;
        $rightToSave = false;
        foreach ($request->answer as $answer) {

            if(array_key_exists($i, $request->right))
            {
                $rightToSave = true;
            }
            $answer = new MultipleChoiceQuestionAnswer([
                'question_id' => $question_id,
                'content' => $answer,
                'explanation' => $request->answer_explanation[$i],
                'right' => $rightToSave
            ]);

            $answer->save();
            $rightToSave = false;
            $i++;
        }

        return true;
    }
}
