<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Http\Requests\CreateInviteRequest;
use Testy\Http\Requests\SearchUserRequest;
use Testy\Models\Group;
use Testy\Models\Invite;
use Testy\Models\User;

class InviteController extends Controller
{
    /**
     * GroupController constructor.
     *
     * JWT auth middleware
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $invites = Auth::user()->invites->each(function($invite) {
            $invite->group;
            $invite->fromUser;
        });

        return view('invite.index', compact('invites'));
    }

    /**
     * Return searched users by the query.
     *
     * @param SearchUserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function search(SearchUserRequest $request)
    {
        $q = $request->q;

        $users = User::where(function ($query) use ($q) {
                $query->where('username','LIKE', "%{$q}%")
                    ->orWhere('name','LIKE', "%{$q}%");
            })
            ->where('id', '!=', Auth::user()->id)
            ->select(['id', 'name', 'username'])
            ->with('invites')
            ->get()
            ->filter(function($user)
            {
                return $user->isThereInvitesFrom(Input::get('group'), Auth::user()->id);
            });

        return response()->json($users);
    }


    /**
     * Handle recieved invite request
     *
     * @param $id
     * @param CreateInviteRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @internal param $ Requests\CreateInviteRequest $
     */
    public function invite($id, CreateInviteRequest $request)
    {
        $group = Group::find($id);

        $this->authorize('inviteUsers', $group);

        $invite = Invite::create([
            'from_user_id' => Auth::user()->id,
            'to_user_id' => $request->user,
            'group_id' => $id,
            'accepted' => false
        ]);

        $invite->save();

        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @internal param Invite $invite
     */
    public function destroy($id)
    {
        $invite = Invite::find($id);
        $invite->delete();

        return back();
    }
}