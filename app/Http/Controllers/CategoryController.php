<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Models\Category;

class CategoryController extends Controller
{
    /**
     * Instantiate new TestController instance.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $query = $request->q;
        $categories = Category::where('category_name', 'LIKE', "%{$query}%")->get();

        return $categories;
    }
}
