<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Models\Question;
use Testy\Models\Result;

class ResultController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $result = Result::find($id);

        $test = $result->test;

        $sequence = json_decode($result->result_sequence, true);

        return view('result.show', compact('result', 'test', 'sequence'));
    }

    /**
     * Fetching questions connected to given test and shuffle if wanted so.
     *
     * @param $test
     * @param null $shuffle
     * @return mixed
     */
    private function fetchQuestions($test, $shuffle = null) {

        if($shuffle == 'no-shuffle') {
            $questions = Question::where('test_id', $test->id)->get()->each(function($question){
                $question->answers->toArray();
            });
            return $questions;
        }

        $questions = Question::where('test_id', $test->id)->get()->shuffle()->each(function($question){
            $question->answers->shuffle()->toArray();
        });

        return $questions;
    }
}
