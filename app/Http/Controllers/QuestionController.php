<?php

namespace Testy\Http\Controllers;

use Barryvdh\DomPDF\PDF;
use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;
use Testy\Infrastructure\FileHandler;
use Testy\Models\Answer;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Http\Requests\CreateQuestionRequest;
use Testy\Models\Question;
use Testy\Models\QuestionType;

class QuestionController extends Controller
{
    /**
     * Instantiate new TestController instance.
     */
    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function selectQuestion(Request $request)
    {
        $testId = $request->test_id;
        $questionTypes = QuestionType::where('active', true)->get();
        return view('question.question-types-partial', compact('questionTypes', 'testId'))->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|CreateQuestionRequest $request
     * @return Response
     */
    public function store(CreateQuestionRequest $request)
    {
        $fileHandler = new FileHandler();
        $fileName = $fileHandler->saveFile($request, 'questions');

        $question = Question::create([
            'test_id' => $request->test_id,
            'question_content' => $request->question_content,
            'question_active' => isset($request->question_active) ? '1' : '0',
            'question_answer_visible' => isset($request->question_answer_visible) ? $request->question_answer_visible : '-1',
            'question_img_url' => $fileName,
            'type' => 'closed'
        ]);

        $question->save();

        $this->saveAnswer($request, $question->id);

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Question $question
     * @return Response
     * @internal param int $id
     */
    public function show(Question $question)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Question $question
     * @return Response
     * @internal param int $id
     */
    public function edit(Question $question)
    {
        return view('question.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CreateQuestionRequest $request
     * @param Question $question
     * @return Response
     * @internal param int $id
     */
    public function update(CreateQuestionRequest $request, Question $question)
    {
        $fileHandler = new FileHandler();
        $fileName = $fileHandler->saveFile($request, 'questions');

        $question->update([
            'test_id' => $request->test_id,
            'question_content' => $request->question_content,
            'question_active' => isset($request->question_active) ? '1' : '0',
            'question_answer_visible' => isset($request->question_answer_visible) ? $request->question_answer_visible : '-1',
            'question_img_url' => $fileName,
            'type' => 'closed'
        ]);

        $this->updateAnswer($request, $question->id);

        return view('question.edit', compact('question', 'img'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Question $question
     * @return Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(Question $question)
    {
        $test_id = $question->test_id;
        $question->delete();

        return view('/test/' . $test_id . '/edit');
    }

    /**
     * Save answers after submitting form
     * @param $request
     * @param $question_id
     * @return boolean
     */
    private function saveAnswer($request, $question_id)
    {
        $i = 1;
        $rightToSave = false;
        foreach ($request->answer as $answer) {
            if(array_key_exists($i, $request->right))
            {
                $rightToSave = true;
            }
            $answer = new Answer(['question_id' => $question_id, 'answer_content' => $answer, 'answer_explanation' => $request->answer_explanation[$i],'answer_right' => $rightToSave]);
            $answer->save();
            $rightToSave = false;
            $i++;
        }

        return true;
    }

    /**
     * Update answers after submitting form
     * @param $request
     * @param $question_id
     * @return boolean
     */
    private function updateAnswer($request, $question_id)
    {
        $i = 1;
        $rightToSave = false;
        foreach ($request->answer as $answer_content) {
            if(array_key_exists($i, $request->right))
            {
                $rightToSave = true;
            }
            $answer = Answer::find($request->answer_id[$i]);

            if($answer != null){
                $answer = Answer::find($request->answer_id[$i])->update(['question_id' => $question_id, 'answer_content' => $answer_content, 'answer_explanation' => $request->answer_explanation[$i],'answer_right' => $rightToSave]);
            } else {
                $answer = new Answer(['question_id' => $question_id, 'answer_content' => $answer_content, 'answer_explanation' => $request->answer_explanation[$i],'answer_right' => $rightToSave]);
                $answer->save();
            }

            $rightToSave = false;
            $i++;
        }

        return true;
    }
}
