<?php

namespace Testy\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Testy\Http\Requests\CreateTestingRequest;

use Illuminate\Support\Facades\Auth;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Http\Requests\CreateTestQuestionDetailsRequest;
use Testy\Http\Requests\CreateTestRequest;
use Testy\Models\Category;
use Testy\Models\Language;
use Testy\Models\Question;
use Testy\Models\Test;
use Testy\Models\Result;
use Testy\Models\TestingSession;
use Testy\Services\TestService;

class TestController extends Controller
{
    /**
     *  Instantiate new TestController instance.
     */
    public function __construct(TestService $testService){
        $this->service = $testService;
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function index(Request $request)
    {
        $tests = $this->service->getIndexTests($request);
        if ($request->ajax()) {
            return view('test.test-index-partial', compact('tests'));
        }
        return view('test.index', compact('tests'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $categories = Category::all();
        $languages = Language::all();
        $test = new Test();

        return view('test.create-test-details', compact('categories', 'test', 'languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|CreateTestRequest $request
     * @return Response
     */
    public function store(CreateTestRequest $request)
    {
        $test = $this->service->create($request);

        return view('test.create-test-question-details', compact('test'));
    }

    /**
     * @param Requests\CreateTestQuestionDetailsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeTestQuestionDetails(CreateTestQuestionDetailsRequest $request, Test $test) {
        if (!$this->service->createTestQuestionDetails($request, $test)) {
            return abort(403, 'Forbidden.');
        }

        return view('test.create-test-questions', compact('test'));
    }

    /**
     * @param Requests\CreateTestQuestionDetailsRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function storeTestQuestions(CreateTestQuestionDetailsRequest $request, Test $test) {
        if (!$this->service->createTestQuestions($request, $test)) {
            return abort(403, 'Forbidden.');
        }

        return view('test.show', compact('test'));
    }

    /**
     * Display the specified resource.
     *
     * @param Test $test
     * @return Response
     * @internal param int $id
     */
    public function show(Test $test)
    {
        if ($test->test_creation_stage == 2)
        {
            $this->authorize('editData', $test);
            return view('test.create-test-question-details', compact('test'));
        }
        else if ($test->test_creation_stage == 3)
        {
            $this->authorize('editData', $test);
            return view('test.create-test-questions', compact('test'));
        }

        return view('test.show', compact('test'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Test $test
     * @return Response
     * @internal param int $id
     */
    public function edit(Test $test)
    {
        $this->authorize('editData', $test);

        $questions = $test->questions();
        $languages = Language::where('active', 1)->get();
        return view('test.edit', compact('test', 'questions', 'languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|CreateTestRequest $request
     * @param Test $test
     * @return Response
     * @internal param int $id
     */
    public function update(CreateTestRequest $request, Test $test)
    {
        $this->authorize('editData', $test);

        $this->service->update($request, $test);

        return back()->with('success', 'Data is updated successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Test $test
     * @return Response
     * @throws \Exception
     * @internal param int $id
     */
    public function destroy(Test $test)
    {
        $this->authorize('editData', $test);

        $test->delete();

        return redirect('/user');
    }

    /**
     * Return testing view.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getTesting($id){

        $session = TestingSession::where('test_id', $id)->where('user_id', Auth::user()->id)->where('completed_at', null)->first();

        $debug = false;

        if (!$session) {
            $test = Test::find($id);
            $test->questions = $test->testingQuestions();

            $testPrep = $test->getAttributes();
            foreach ($test->questions as $index => $question)
            {
                $testPrep['questions'][$index] = $question->prepareSolving();
                dd($testPrep['questions'][$index]);
                $testPrep['questions'][$index]['question_reference']['question_type'] = $question->question_reference->question_type;
            };

            $json = json_encode($testPrep, JSON_FORCE_OBJECT);
            TestingSession::create([
                'test_id' => $id,
                'user_id' => Auth::user()->id,
                'test_state' => $json,
                'start_time' => Carbon::now(),
                'end_time' => Carbon::now()->addHours(4),
                'completed_at' => null,
            ]);
            $test = (object) json_decode($json, false);
        } else {
            $test = (object) json_decode($session->test_state, false);
        }

        return view('testing.index', compact('test', 'debug'));
    }

    /**
     * Remove image from test
     * @param Test $test
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function removeImage(Test $test) {
        if (!$this->service->removeImage($test)){
            return response(500);
        }
        return response(200);
    }

    /**
     * Add image to test
     * @param Request $request
     * @param Test $test
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function addImage(Request $request, Test $test) {
        if (!$this->service->addImage($request, $test)) {
            return response(500);
        }
        return response(['status' => 200, 'file_name' => $test->test_img_url]);
    }

    /**
     * Get POST request and return result.
     *
     * @param $id
     * @param CreateTestingRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postTesting($id, CreateTestingRequest $request)
    {
        dd($request->all());
        $session = TestingSession::where('test_id', $id)->where('user_id', Auth::user()->id)->where('completed_at', null)->first();

        dd(json_decode($session->test_state, false));

        $session->update([
            'test_state' => '',
            'completed_at' => Carbon::now(),
        ]);

        return redirect('/test/' . $id);
    }
}
