<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;
use Testy\Http\Requests\CreateOpenQuestionRequest;
use Testy\Infrastructure\FileHandler;
use Testy\Models\OpenQuestion;
use Testy\Models\OpenQuestionAnswer;
use Testy\Models\Question;
use Testy\Models;

class OpenQuestionController extends Controller
{
    public function store(CreateOpenQuestionRequest $request) {
        $fileHandler = new FileHandler();
        $fileName = $fileHandler->saveFile($request->img, 'open-questions');
        $exact = false;

        if ($request->exact != null && $request->exact == 'on')
        {
            $exact = true;
        }

        $openQuestion = OpenQuestion::create([
            'content' => $request['content'],
            'active' => isset($request->active) ? '1' : '0',
            'img_url' => $fileName,
            'exact' => $exact
        ]);

        $openQuestion->save();

        $questionRefenrece = Question::create([
            'test_id' => $request->test_id,
            'question_type_id' => $request->type_id,
            'question_id' => $openQuestion->id
        ]);

        $questionRefenrece->save();

        if ($request->answer) {
            $this->saveAnswer($request, $openQuestion->id);
        }

        return response(200);
    }

    /**
     * Save answers after submitting form
     * @param $request
     * @param $question_id
     * @return boolean
     */
    private function saveAnswer($request, $question_id)
    {
        $i = 1;
        foreach ($request->answer as $answer) {

            $answer = new OpenQuestionAnswer([
                'question_id' => $question_id,
                'content' => $answer,
                'explanation' => $request->answer_explanation[$i],
            ]);

            $answer->save();
            $i++;
        }

        return true;
    }
}
