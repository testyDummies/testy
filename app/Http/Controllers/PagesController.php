<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Testy\Http\Requests;
use Testy\Http\Controllers\Controller;
use Testy\Models\Test;
use Testy\Models\User;

class PagesController extends Controller
{
    public function home() {
        if (!Auth::user())
        {
            return response()->view('landing');
        }
        $lastTests = Test::where('active', 1)->orWhere('active', 0)->orderBy('created_at', 'desc')->take(10)->get();

        $topTests = Test::where('active', 1)->orWhere('active', 0)->orderBy('user_id', 'desc')->take(10)->get();

        $topUsers = User::orderBy('id', 'desc')->take(10)->get();
        return view('home', compact('lastTests', 'topTests', 'topUsers'));
    }

    public function modal() {
        return response(view('account.modals.common-test')->render());
    }
}
