<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;

use Testy\Http\Requests;
use Testy\Models\Answer;

class AnswerController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param Answer $answer
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Answer $answer)
    {
        $answer->delete();

        return redirect()->back();
    }
}
