<?php

namespace Testy\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use Testy\Http\Requests;
use Testy\Models\Question;
use Testy\Models\Test;

class FileController extends Controller
{
    public function generate($id) {
        $test = Test::find($id);
        $questions = $test->questions();
        $view = view('generator.index', compact('questions'))->render();
        //dd($view);
        return \PDF::loadHTML($view)->setPaper('a4')->setWarnings(false)->stream();
    }


    /**
     * Fetching questions connected to given test and shuffle if wanted so.
     *
     * @param $test
     * @param bool $shuffleQuestions
     * @param bool $shuffleAnswers
     * @return mixed
     * @internal param null $shuffle
     */
    private function fetchQuestions($test, $shuffleQuestions = true, $shuffleAnswers = true) {
        $questions = Question::where('test_id', $test->id)
            ->get()
            ->take($test->test_displayed_questions <= 0 ? $test->questions->count() : $test->test_displayed_questions );

        if($shuffleQuestions) {
            $questions = $questions->shuffle();
        }

        if($shuffleAnswers) {
            $questions = $questions->each(function($question){
                $question
                    ->answers
                    ->shuffle()
                    ->toArray();
            });
        } else {
            $questions = $questions->each(function($question){
                $question->answers->toArray();
            });
        }
        return $questions;
    }
}
