<?php

namespace Testy\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMultipleChoiceQuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'content' => 'required|min:5|max:255',
            'answer' => 'required|array',
            'right' => 'required',
            'img' => 'image',
            'answer_visible' => 'integer'
        ];
    }
}
