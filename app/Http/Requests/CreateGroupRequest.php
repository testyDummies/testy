<?php

namespace Testy\Http\Requests;

use Testy\Http\Requests\Request;

class CreateGroupRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'group_name' => 'required|min:3|max:120',
            'group_description' => 'required|min:5'
        ];
    }
}