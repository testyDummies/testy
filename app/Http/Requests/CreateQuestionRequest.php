<?php

namespace Testy\Http\Requests;

use Testy\Http\Requests\Request;

class CreateQuestionRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'question_content' => 'required|min:5|max:255',
            'answer' => 'required|array',
            'right' => 'required',
            'question_img' => 'image',
            'question_answer_visible' => 'integer'
        ];
    }
}
