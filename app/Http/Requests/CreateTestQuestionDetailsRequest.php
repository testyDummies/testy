<?php

namespace Testy\Http\Requests;

use Testy\Http\Requests\Request;

class CreateTestQuestionDetailsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'test_displayed_questions'  => 'integer|min:1'
        ];
    }
}
