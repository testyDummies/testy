<?php

namespace Testy\Http\Requests;

use Testy\Http\Requests\Request;

class CreateTestRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'test_name'                 => 'required|min:3|max:120',
            'test_short_description'    => 'min:10|max:100',
            'test_description'          => 'required|min:10',
            'language_id'               => 'required|integer',
            'categories'                => 'required|array',
            'test_image'                => 'image|max:1024',
            'tags'                      => 'array'
        ];
    }
}
