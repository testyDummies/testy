<?php

namespace Testy\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Testy\Models\EarlySubscriber;

class NewEarlySubscriber extends Mailable
{
    use Queueable, SerializesModels;

    private $sub;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(EarlySubscriber $sub)
    {
        $this->sub = $sub;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $sub = $this->sub;
        return $this->view('mails.subscriber.thank-you', compact('sub'));
    }
}
