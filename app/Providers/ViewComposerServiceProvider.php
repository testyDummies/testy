<?php

namespace Testy\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Testy\Models\User;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
//        dd(Auth::user());
//        if(Auth::check())
//        {
        view()->composer('shared.nav', function($view)
        {
            //dd(Auth::user()->invites->count());
            $view->with('invites_count', $this->getInvited());
        });
    }

    private function getInvited()
    {
        if (\Auth::check())
        {
            return Auth::user()->invites->count();
        }
        else
        {
            return 0;
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
