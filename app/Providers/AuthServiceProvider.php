<?php
namespace Testy \Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Testy\Models\Group;
use Testy\Models\Test;
use Testy\Models\User;
use Testy\Policies\InvitePolicy;
use Testy\Policies\TestPolicy;
use Testy\Policies\UserPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Test::class => TestPolicy::class,
	Group::class => InvitePolicy::class,
        User::class => UserPolicy::class,
    ];
    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        //
    }
}
