<?php

namespace Testy\Models;

use Illuminate\Database\Eloquent\Model;

class TestingSession extends Model
{
    protected $fillable = [
        'test_id',
        'user_id',
        'test_state',
        'start_time',
        'end_time',
        'completed_at',
    ];
}
