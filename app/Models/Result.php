<?php

namespace Testy\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'test_id',
        'result_sequence',
        'result_percent',
        'result_points'
    ];

    /**
     * Return user associated with given result
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->belongsTo('Testy\Models\User', 'user_id', 'id');
    }

    /**
     * Return test associated with given result.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function test()
    {
        return $this->belongsTo('Testy\Models\Test', 'test_id', 'id');
    }
}
