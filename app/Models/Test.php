<?php

namespace Testy\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Test extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'test_name',
        'test_short_description',
        'test_description',
        'test_displayed_questions',
        'test_img_url',
        'user_id',
        'language_id',
        'test_creation_stage',
        'active',
        'test_shuffle_questions'
    ];

    /**
     * Get categories associated with the given test.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany('Testy\Models\Category')->withTimestamps();
    }

    /**
     * Get tags associated with given test.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany('Testy\Models\Tag')->withTimestamps();
    }

    /**
     * Return Result associated with given test.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany('Testy\Models\Result');
    }

    /**
     * Return questions related to a test
     *
     * @return array
     */
    public function questions()
    {
        $questionReferences = $this->hasMany('Testy\Models\Question');
        $questionReferences = $questionReferences->getResults();

        $questions = [];
        foreach ($questionReferences as $questionReference) {
            $class = new \ReflectionClass($questionReference->question_type->table_name);
            $model = $class->newInstanceWithoutConstructor();
            array_push($questions, $model->where('id', $questionReference->question_id)->first());
        }

        return $questions;
    }

    /**
     * Return questions related to a test
     *
     * @return array
     */
    public function testingQuestions()
    {
        $questionReferences = $this->hasMany('Testy\Models\Question');
        $questionReferences = $questionReferences->getResults();

        $questions = [];
        foreach ($questionReferences as $questionReference) {
            $class = new \ReflectionClass($questionReference->question_type->table_name);
            $model = $class->newInstanceWithoutConstructor();
            array_push($questions, $model->where('id', $questionReference->question_id)->first()->getTestingForm());
        }
        return $questions;
    }

    /**
     * Return if test is active
     *
     * @return bool
     */
    public function isActive()
    {
        return count($this->questions()) > 0;
    }

    /**
     * Return if user has images
     *
     * @return bool
     */
    public function hasImage()
    {
        if (!$this->test_img_url || $this->test_img_url == '') {
            return false;
        }
        return true;
    }

    /**
     * Return if current user is owner
     *
     * @return bool
     */
    public function isUserOwner()
    {
        return $this->user_id == Auth::user()->id;
    }

    /**
     * @return int
     */
    public function displayed_questions() {
        return (int)$this->test_displayed_questions < 3 ? $this->questions->count() : (int)$this->test_displayed_questions;
    }
}
