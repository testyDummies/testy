<?php

namespace Testy\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'test_id',
        'question_content',
        'question_active',
        'question_answer_visible',
        'question_img_url',
        'type',
        'question_type_id',
        'question_id'
    ];

    public function question_type() {
        return $this->hasOne(QuestionType::class, 'id', 'question_type_id');
    }
}
