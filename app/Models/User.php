<?php

namespace Testy\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'role'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Return result associated with given user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany('Testy\Models\Result');
    }


    /**
     * Return invites associated with given user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invites()
    {
        return $this->hasMany('Testy\Models\Invite', 'to_user_id');
    }

    /**
     * Return groups associated with given user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function groups()
    {
        return $this->belongsToMany('Testy\Models\Group');
    }

    /**
     * Return if user has joined in given group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inGroup($group_id)
    {
        return $this->groups()->where('group_id', $group_id)->count() == 1;
    }

    /**
     * Return if user is invited already.
     *
     * @param $group_id
     * @param $user_id
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function isThereInvitesFrom($group_id, $user_id)
    {
        return $this->invites()->where('from_user_id', $user_id)->where('group_id', $group_id)->get()->count() === 0;
    }
}
