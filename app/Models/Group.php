<?php

namespace Testy\Models;

use Illuminate\Database\Eloquent\Model;
use Testy\Policies\InvitePolicy;

class Group extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'group_name',
        'group_description',
        'group_type',
        'group_owner_id'
    ];

    /**
     * Get users associated with the given group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('Testy\Models\User')->withTimestamps();
    }

    /**
     * Get tests associated with the given group.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tests()
    {
        return $this->belongsToMany('Testy\Models\Test')->withTimestamps();
    }
}