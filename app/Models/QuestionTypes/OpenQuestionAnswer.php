<?php

namespace Testy\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;

class OpenQuestionAnswer extends Model
{
    protected $fillable = [
        'question_id',
        'content',
        'explanation'
    ];
}
