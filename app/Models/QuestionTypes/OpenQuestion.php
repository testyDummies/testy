<?php

namespace Testy\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;
use Testy\Models;

class OpenQuestion extends BaseQuestion
{
    protected $fillable = [
        'content',
        'active',
        'img_url',
        'exact_answer'.
        'question_reference_id'
    ];


    public function answers()
    {
        return $this->hasMany(OpenQuestionAnswer::class, 'question_id');
    }

    public function getTestingForm()
    {
        return $this;
    }
}
