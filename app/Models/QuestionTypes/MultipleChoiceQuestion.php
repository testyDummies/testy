<?php

namespace Testy\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;

class MultipleChoiceQuestion extends BaseQuestion
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'active',
        'answers_visible',
        'img_url'.
        'question_reference_id'
    ];

    public function answers()
    {
        return $this->hasMany(MultipleChoiceQuestionAnswer::class, 'question_id', 'id');
    }

    public function getTestingForm()
    {
        $answersToTake = (int)$this->answers_visible == 0 ? $this->answers->count() : (int)$this->answers_visible;

        $rightAnswerToAdd = $this
            ->answers()
            ->where('right', 1)
            ->take(1)
            ->first();

        $this->answers = $this
            ->answers
            ->where('right', 0)
            ->shuffle()
            ->take($answersToTake - 1)
            ->prepend($rightAnswerToAdd)
            ->shuffle();

        return $this;
    }

    public function prepareSolving()
    {
        $answerPrep = $this->getAttributes();

        foreach ($this->answers as $index => $answer)
        {
            $answerPrep['answers'][$index] = $answer->toArray();
        }
        return $answerPrep;
    }
}
