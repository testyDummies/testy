<?php
namespace Testy\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;
use Testy\Models\Question;

abstract class BaseQuestion extends Model {
    abstract public function getTestingForm();

    public function question_reference()
    {
        return $this->hasOne(Question::class, 'id', 'question_reference_id');
    }

    public function hasImage()
    {
        return $this->img_url && $this->img_url != '';
    }

    public function prepareSolving()
    {
        return $this;
    }
}