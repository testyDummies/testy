<?php

namespace Testy\Models\QuestionTypes;

use Illuminate\Database\Eloquent\Model;

class MultipleChoiceQuestionAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'content',
        'explanation',
        'right'
    ];
}
