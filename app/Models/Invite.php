<?php

namespace Testy\Models;

use Illuminate\Database\Eloquent\Model;

class Invite extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'from_user_id',
        'to_user_id',
        'group_id',
        'accepted',
    ];

    public function group()
    {
        return $this->belongsTo('Testy\Models\Group');
    }

    public function fromUser()
    {
        return $this->belongsTo('Testy\Models\User', 'from_user_id', 'id');
    }
}