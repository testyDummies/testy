<?php

namespace Testy\Models;

use Illuminate\Database\Eloquent\Model;

class EarlySubscriber extends Model
{
    protected $fillable = [
        'name',
        'email'
    ];
}
