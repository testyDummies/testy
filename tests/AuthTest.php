<?php

use Illuminate\Foundation\Auth\User;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class AuthTest extends TestCase
{
    /**
     * Unauthorized landing page test.
     *
     * @return void
     */
    public function testUnauthorized()
    {
        $this->visit('/')->see('Landing');
    }

    /**
     * Check if authorize with correct credentials works.
     *
     * @return void
     */
    public function testAuthorizedSuccess() {
        $this->visit('/auth/login')
            ->type('v.chalamov@abv.bg', 'email')
            ->type('1q2w3e4r', 'password')
            ->press('Submit')
            ->seePageIs('/home');
    }


    /**
     * Check if authorize with NOT correct credentials works.
     *
     * @return void
     */
    public function testAuthorizedFailed() {
        $this->visit('/auth/login')
            ->type('v.chalamov@abv.bg', 'email')
            ->type('failedPass', 'password')
            ->press('Submit')
            ->seePageIs('/auth/login');
    }
}
