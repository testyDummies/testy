<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Testy\Models\User;

class TestTest extends TestCase
{

    /**
     * A basic test browse.
     *
     * @return void
     */
    public function testBrowseTests()
    {
        $this->visit('/test')->seePageIs('/test');
    }

    public function testCreateTestNonAuth() {
        $this->visit('/test/create')->seePageIs('/auth/login');
    }

    public function testCreateTestAuth() {
        $user = new User(['name' => 'John']);

        $this->be($user);

        $this->visit('/test/create')->seePageIs('/test/create');
    }
}
