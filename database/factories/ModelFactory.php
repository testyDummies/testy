<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Testy\Models\User::class, function ($faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->email,
        'username' => $faker->username,
        'password' => bcrypt('1q2w3e4r'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Testy\Models\Test::class, function ($faker) {
    return [
        'test_name' => $faker->sentence,
        'test_description' => $faker->paragraph,
        'category_id' => $faker->numberBetween($min = 1, $max = 15),
        'user_id' => $faker->numberBetween($min = 1, $max = 25)
    ];
});
$factory->define(Testy\Models\Question::class, function ($faker) {
    return [
        'test_id' => $faker->numberBetween($min = 1, $max = 25),
        'question_content' => $faker->paragraph,
        'type' => 'closed'
    ];
});
$factory->define(Testy\Models\Answer::class, function ($faker) {
    return [
        'question_id' => $faker->numberBetween($min = 1, $max = 250),
        'answer_content' => $faker->paragraph,
        'answer_explanation' => $faker->paragraph,
        'answer_right' => $faker->numberBetween($min = 0, $max = 1)
    ];
});

