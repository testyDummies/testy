<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpenQuestionAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('open_question_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned();;
            $table->foreign('question_id')->references('id')->on('open_questions')->onDelete('cascade');
            $table->string('content');
            $table->string('explanation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('open_question_answers');
    }
}
