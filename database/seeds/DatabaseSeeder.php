<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        \Testy\Models\QuestionType::create([
            'display_name' => 'Multiple choice',
            'table_name' => 'multiple_choice_questions',
            'active' => true,
            'premium' => false
        ]);

//        $category = \Testy\Models\Category::create([
//            'category_name' => 'History',
//            'category_tests' => 0
//        ]);
        // $this->call(UserTableSeeder::class);

        Model::reguard();
    }
}
