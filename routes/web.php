<?php
// Authentication routes...
Route::post('/subscriber', 'PagesController@home');

Auth::routes();
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/', 'PagesController@home');
Route::get('/home', 'PagesController@home');

// Test Routes
Route::model('/test', 'Test');
Route::post('/test/test-question-details/{test}', 'TestController@storeTestQuestionDetails');
Route::post('/test/test-questions/{test}', 'TestController@storeTestQuestions');
Route::post('/test/{test}/image', 'TestController@addImage');
Route::delete('/test/{test}/image', 'TestController@removeImage');
Route::resource('/test', 'TestController');

Route::model('/question', 'Question');
Route::get('/question/types', 'QuestionController@selectQuestion');
Route::resource('/question', 'QuestionController');

Route::model('/question-type', 'QuestionType');
Route::get('/question-type/{questionType}', 'QuestionTypeController@show');

Route::model('/group', 'Group');
Route::get('/group/{id}/user/join', 'GroupController@joinUser', 'Group');
Route::get('/group/{id}/test/join', 'GroupController@joinTest', 'Group');
Route::resource('/group', 'GroupController');

Route::resource('/result', 'ResultController');
Route::resource('/answer', 'AnswerController');

// Multiple choice routes
Route::post('/multiple-choice-question', 'MultipleChoiceQuestionController@store');

// Multiple choice routes
Route::post('/open-question', 'OpenQuestionController@store');

// Do a test
Route::get('/test/{id}/testing', 'TestController@getTesting');
Route::post('/test/{id}/testing', 'TestController@postTesting');

//Categories
Route::get('/categories', 'CategoryController@index');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Profile routes...
Route::get('/user/{username?}', 'ProfileController@index');


Route::get('profile/modal', 'AccountController@getModal');

Route::post('profile/changeName', 'AccountController@changeName');
Route::post('profile/changeEmail', 'AccountController@changeEmail');
Route::post('profile/changePassword', 'AccountController@changePassword');
Route::post('profile/changeUsername', 'AccountController@changeUsername');

//File actions
Route::get('/file/{id}', 'FileController@generate');

// Invite users
Route::get('/invite', 'InviteController@index');
Route::delete('/invite/{id}', 'InviteController@destroy');

Route::get('/invite/user', 'InviteController@search');
Route::get('/invite/user/{group_id}', 'InviteController@invite');


Route::get('/test/modal', 'PagesController@modal');
Auth::routes();