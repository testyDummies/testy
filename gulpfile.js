var elixir = require('laravel-elixir');
require('laravel-elixir-livereload');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix
    //.sass('app.scss')
    .sass('landing.scss')
    .scripts([
        // Combine all js files into one.
        // Path is relative to resource/js folder.
        '../../../node_modules/jquery/dist/jquery.min.js',
        '../../../node_modules/bootstrap/dist/js/bootstrap.js',
        '../../../node_modules/tinymce/jquery.tinymce.min.js',
        '../../../node_modules/tinymce/tinymce.min.js',
        '../../../node_modules/tinymce/themes/modern/theme.min.js',
        '../../../node_modules/blueimp-file-upload/js/vendor/jquery.ui.widget.js',
        '../../../node_modules/blueimp-file-upload/js/jquery.iframe-transport.js',
        '../../../node_modules/blueimp-file-upload/js/jquery.fileupload.js',
        // '../../../node_modules/blueimp-file-upload/js/jquery.fileupload-ui.js',
        // '../../../node_modules/blueimp-file-upload/js/jquery.fileupload-image.js',
        '../**/*.js',
    ], 'public/js/app.js');
    mix.livereload(['app/**/*', 'public/**/*', 'resources/views/**/*']) // 'resources/assets/**/*'
});
