//$(document).ready(function() {
//    $('#changeNameForm').on('submit', function(e) {
//        e.preventDefault();
//        changeName($('#changeNameForm'));
//    });
//});
//function changeName($form){
//    var data = {
//        'newName': $form.find('#newName').val(),
//        'password': $form.find('#password').val(),
//        '_token': $form.find('input[name=_token]').val()
//    };
//    $.ajax({
//        type: $form.attr('method'),
//        url: $form.attr('action'),
//        data: data,
//        beforeSend: function(){
//            $("#loading").fadeIn(100);
//            $form.find('button[type="submit"]')
//                .attr('disabled', 'disabled');
//        }
//    })
//        .done(function(data) {
//            $form.find('button[type="submit"]').removeAttr('disabled');
//            $("#loading").fadeOut(100);
//            var updated = data.updated == false ? "danger" : "success";
//            $form.find('.alert')
//                .removeClass('alert-success')
//                .removeClass('alert-danger')
//                .addClass('alert-' + updated)
//                .removeClass('hidden')
//                .find('strong')
//                .text(data.message);
//            $('#navFullName').text($('#newName').val());
//            $('*[data-target="#modalName"]').text($('#newName').val());
//        })
//        .fail(function() {
//            $form.find('.alert')
//                .removeClass('alert-success')
//                .addClass('alert-danger')
//                .removeClass('hidden')
//                .find('strong')
//                .text('There wes a problem updating your data :( . Please try again later.');
//        })
//        .always(function() {
//            $form.find('#password').val("");
//        });
//}

function updateModal(url, data) {
    data = data === undefined ? {} : data;
    $.ajax({
        method: "GET",
        url: url,
        data: data
    }).done(function(data) {
        $('#commonModal .modal-content').html(data);
        $('#commonModal').modal('show');
    });
}
