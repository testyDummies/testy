$(document).ready(function() {
    $('#categories').select2({
        maximumSelectionLength: 3
    });

    $('#tags').select2({
        tags: true,
        maximumSelectionLength: 20,
        minimumInputLength: 1,
        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
    });
});