// store the currently selected tab in the hash value
$("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
    var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
});

// on load of the page: switch to the currently selected tab
var hash = window.location.hash;
$('#testEditTabNav a[href="' + hash + '"]').tab('show');

$(function () {
    var $imageContainer = $('#test_image_show');

    $('#test_image').fileupload({
        dataType: 'json',
        formData: {
          _token: $('[name="_token"]').val()
        },
        done: function (e, data) {
            var $img = $imageContainer.find('.hasImage img');
            var src = $img.attr('src');
            var newSrc = src.split('/');
            newSrc.pop();
            newSrc.push(data.result.file_name);
            $img.attr('src', newSrc.join('/'));
            $imageContainer.find('.hasImage').removeClass('hidden');
            $imageContainer.find('.noImage').addClass('hidden');
        }
    });
    $('.container').on('click', '#removeTestImage', function(e) {
        e.preventDefault();
        $.ajax({
            method: 'DELETE',
            url: $(this).attr('data-url'),
            data: {
                _token: $(this).attr('data-token')
            }
        }).done(function() {
            $imageContainer.find('.hasImage').addClass('hidden');
            $imageContainer.find('.noImage').removeClass('hidden');
        }).error(function() {
            alert('Error happened, image not deleted.');
        });
    });
});