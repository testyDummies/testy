var testPage = 1,
    testSort = "none";
$(window).on('hashchange',function(){
    var page = window.location.hash.replace('#','');
    getProducts(page);
    testPage = page;
});

$(document).ready(function() {
    $(document).on('click', '.pagination a', function (e) {
        e.preventDefault();
        var page = $(this).attr('href').split('page=')[1];
        // getProducts(page);
        location.hash = page;
    });

    if(location.hash != 1){
        var hash = location.hash;
        location.hash  = "";
        location.hash = hash;
    }
});

function getProducts(page){
    $.ajax({
        url: '/test?page=' + page + '&sort=' + testSort
    }).done(function(data){
        $('.tests').html(data);

        console.log(data);
    }).fail(function() {

    }).always(function() {

    });
}