$(document).ready(function() {
    $('#deleteQuestionForm').on('submit', function() {
        if(confirm("Do you really want to delete this test?")){
            return true;
        }

        return false;
    });

    $('[type="radio"].sort').on('change', function() {
        testSort = $(this).attr('value');
        getProducts(testPage);
    });
});