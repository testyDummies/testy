$(document).ready(function() {
    $('.delete-question-form').on('submit', function(e) {
        e.preventDefault();

        deleteQuestion($(this));
    });
});

function deleteQuestion($form) {
    var data = {
        '_method': $form.find('input[name=_method]').val(),
        '_token': $form.find('input[name=_token]').val()
    };
    $.ajax({
        type: $form.attr('method'),
        url: $form.attr('action'),
        data: data
    })
        .done(function(data) {
            $form.fadeOut(300, function() {
                $(this).parent().parent().remove();
            });
        })
        .fail(function() {
            console.log('fails');
        })
        .always(function() {
            // TODO:
        });
}