$(document).ready(function() {
    // $(document).on('click', '#addAnswer, .addAnswer',function(e) {
    //     e.preventDefault();
    //     addAnswerInput($(this));
    // });

    // $(document).on('click', 'a.delete-answer-btn', function(e) {
    //     e.preventDefault();
    //     $(this).parent().parent().parent().remove();
    // });
});
function addAnswerInput($element) {
    $answers = $element.parent().parent().parent().find('.answersInput');

    $answerCounter = $element.parent().parent().parent().find('[name^="answer"]').last().attr('name').split("[");

    $answerCounter = $answerCounter[1].split("]");

    $answerCounter = $answerCounter[0]++;

    $answerCounter++;

    $answers.append('<input type="hidden" name="answer_id[' + $answerCounter + ']" value="{{ $answer->id }}">' +
            '<div class="answer-group">' +
                '<div class="input-group">' +
                    '<span class="input-group-addon">' +
                        '<input type="checkbox" name="right[' + $answerCounter + ']">' +
                    '</span>' +
                    '<textarea class="form-control" rows="2" name="answer[' + $answerCounter + ']" placeholder="Answer ' + $answerCounter + '"></textarea>' +
                    '<span class="input-group-addon">' +
                        '<a href="#" class="delete-answer-btn btn btn-danger pull-right">' +
                    '<span class="glyphicon glyphicon-trash"> </span></a></span>' +
                '</div>' +
                '<div class="col-lg-10 col-lg-offset-2">' +
                    '<div class="input-group">' +
                        '<span class="input-group-addon">' +
                            '<span class="glyphicon glyphicon-info-sign"></span>' +
                        '</span>'+
                        '<textarea class="form-control" rows="2" name="answer_explanation[' + $answerCounter + ']" ' + 'placeholder="Explanation for question ' + $answerCounter + '"></textarea>' +
                    '</div>' +
                '</div>' +
            '</div>');

    return true;
}
