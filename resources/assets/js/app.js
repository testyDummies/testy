$(document).ready(function() {
    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus();
    });

    $('[data-toggle="popover"]').popover();

    $(document).on('click', '.popover-link', function(event) {
        event.preventDefault();
        return false;
    });
    $('body').on('hidden.bs.modal', '.modal', function () {
        $(this).removeData('bs.modal');
    });

    // $('input[type="checkbox"].switcher').each(function() {
    //     $(this).attr('type', 'text');
    // });
    //
    // $(document).on('click', 'input[type="checkbox"].switcher', function (e) {
    //     e.preventDefault();
    //     var $this = $(this);
    //     if($this.val() == 'on') {
    //         $this.val('off')
    //     }
    //     else
    //     {
    //         $this.val('on')
    //     }
    // })
});

function ConfirmDelete()
{
    var x = confirm("Are you sure you want to delete?");
    if(x) {
        return true;
    } else {
        return false;
    }
}
tinymce.init({
    selector: '#test_description',
    height: 250,
    menubar: false,
    plugins: [
        // 'advlist autolink lists link image charmap print preview anchor',
        // 'searchreplace visualblocks code fullscreen',
        // 'insertdatetime media table contextmenu paste code'
    ],
    toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    content_css: '//www.tinymce.com/css/codepen.min.css'
});