$(function() {
   $('#addQuestionTestCreate').on('click', function() {
       $.get('/question/select-question-view').done(function (data) {
           $('#addQuestionTestModal .modal-content .modal-body').html(data);
           $('#addQuestionTestModal').modal('show');
       });
   });
});


function updateQuestionModal(url, data) {
    data = data === undefined ? {} : data;
    $.ajax({
        method: "GET",
        url: url,
        data: data
    })
        .done(function(data) {
            var $questionModal = $('#addQuestionTestModal');
            $questionModal.find('.modal-content').html(data);
            $questionModal.modal('show');
        });
}

function loadQuestionForm(url, data) {
    data = data === undefined ? {} : data;
    $.ajax({
        method: "GET",
        url: url,
        data: data
    }).done(function(data) {
        var $questionModal = $('#addQuestionTestModal');
        $questionModal.find('.modal-content .question-content').html(data);
        $questionModal.modal('show');
    });
}