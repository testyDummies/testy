$(function() {
    $('body').on('submit', '#addOpenQuestion', function (e) {
        e.preventDefault();
        var url = $(this).attr('action');
        $.ajax({
            method: "POST",
            url: url,
            data: $(this).serialize()
        }).done(function() {
            var $questionModal = $('#addQuestionTestModal');
            $questionModal.find('.modal-content .question-content').html('');
            $questionModal.modal('hide');
        });
    });
});