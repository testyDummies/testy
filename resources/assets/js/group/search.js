$(document).ready(function() {
    var $userGroupSearch = $('#userGroupSearch');
    var group_id = $('#group').attr('data-group-id');
    var searchQuery = "";
    // Save current value of element
    $userGroupSearch.data('old-value', $userGroupSearch.val());

    // Look for changes in the value

    $userGroupSearch.on('propertychange change click keyup input paste', function() {
        // If value has changed...
        if ($userGroupSearch.data('old-value') != $userGroupSearch.val()) {
            // Updated stored value
            $userGroupSearch.data('old-value', $userGroupSearch.val());

            searchForUsers($userGroupSearch.val());
        }
    });

    function searchForUsers(data) {
        if(data.length > 2) {
            $.ajax({
                type: "GET",
                url: "/invite/user?q=" + data + '&group=' + group_id
            }).done(function (users) {
                $('#userlist li').remove();
                if(typeof users === 'object') {
                    for(var i = 0; i < users.length; i++){
                        $('#userlist').append('<li>' + users[i].name + '(' + users[i].username + ')' +
                            '<a href="/invite/user/' + group_id + '?user=' + users[i].id + '" class="btn btn-success invite"><span class="glyphicon glyphicon-plus"></span></a></li>');
                    }
                }
            }).fail(function (jqXHR, textStatus, errorThrown) {
                console.log("AJAX call failed: " + textStatus + ", " + errorThrown);
            });
        }
    }

    $('#inviteUser').on('click', 'a.invite', function(event) {
        event.preventDefault();
        $.get($(this).attr('href'), function() {})
            .done(function(data) {
                searchForUsers($userGroupSearch.data('old-value'));
            })
            .fail(function() {
            })
            .always(function() {
            });
    });
});

