@extends('shared.layout')

@section('content')
    <div class="well bs-component">
        @include('test.partials.creation-loading', ['stage'=>1])
        <form class="form-horizontal" method="POST" action="/test"  enctype="multipart/form-data"
              xmlns:v-on="http://www.w3.org/1999/xhtml">
            <fieldset>
                <legend>Create Test</legend>
                <div class="form-group">
                    <label for="test_name" class="col-lg-2 control-label">Name of the test</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="test_name" name="test_name" placeholder="Name of the test" value="{{ old('test_name') ?: '' }}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="test_short_description" class="col-lg-2 control-label">Short Description</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="3" name="test_short_description" id="test_short_description">{{ old('test_short_description') ?: '' }}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="test_description" class="col-lg-2 control-label">Description</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="3" name="test_description" id="test_description">{{ old('test_description') ?: '' }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="language_id" class="col-lg-2 control-label">Test language</label>

                    <div class="col-lg-10">
                        <select name="language_id" id="language_id" class="form-control language">
                            @foreach($languages as $language)
                                <option value="{{ $language->id }}">{{ $language->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="categories" class="col-lg-2 control-label">Choose category</label>

                    <div class="col-lg-10">
                        <select name="categories[]" id="categories" class="form-control category-select" multiple="multiple">
                            {{--<strong class="select2-results__group">Top categories:</strong>--}}
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="tags" class="col-lg-2 control-label">Tags</label>

                    <div class="col-lg-10">
                        <select name="tags[]" id="tags" class="form-control tags" multiple="multiple">
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="test_image" class="col-lg-2 control-label">Test image</label>

                    <div class="col-lg-10">
                        <input type="file" id="test_image" class="form-control" name="test_image">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
                {!! csrf_field() !!}
            </fieldset>
        </form>
    </div>
@endsection