<div class="row">
    <div class="col-md-4"><h4>Test Details</h4></div>
    <div class="col-md-4"><h4>Question Details</h4></div>
    <div class="col-md-4"><h4>Questions Creation</h4></div>
    <div class="col-md-12">
        <div class="progress">
            <div class="progress-bar progress-bar-success" style="width: 33.33333%"></div>
            <div class="progress-bar progress-bar-warning" style="{{ $stage > 1 ? 'width: 33.33333%' : '' }}"></div>
            <div class="progress-bar progress-bar-danger" style="{{ $stage == 3 ? 'width: 33.33333%' : '' }}"></div>
        </div>
    </div>
</div>