@include('test.partials.add-question-button')

<br>

<div id="editQuestions">
    @foreach($questions as $question)
        <div class="panel panel-success">
            @include('question.' . (new ReflectionClass($question))->getShortName() . '.show-partial')
        </div>
    @endforeach
</div>