
<form action="/test/{{ $test->id }}" method="post" class="form-horizontal" enctype="multipart/form-data">
    {!! method_field('PUT') !!}
    {!! csrf_field() !!}
    <fieldset>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="test_name" class="control-label">Test name</label>
                <input type="text" class="form-control" name="test_name" value="{{ $test->test_name }}" id="test_name">
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="test_short_description" class="control-label">Short Descripton</label>
                <textarea class="form-control" name="test_short_description" id="test_short_description">{{ $test->test_short_description }}</textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="test_description" class="control-label">Description</label>
                <textarea class="form-control" name="test_description" id="test_description">{{ $test->test_description }}</textarea>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12">
                <label for="inputEmail" class="control-label">Test language</label>
                <select name="language_id" id="language_id" class="form-control language">
                    @foreach($languages as $language)
                        <option value="{{ $language->id }}">{{ $language->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12">
                <label for="inputEmail" class="control-label">Categories</label>
                <select name="categories[]" id="categories" class="form-control category-select" multiple="multiple">
                    @foreach($test->categories as $category)
                        <option value="{{ $category->id }}" selected>{{ $category->category_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label for="inputEmail" class="control-label">Tags</label>

                <select name="tags[]" id="tags" class="form-control tags" multiple="multiple">
                    @foreach($test->tags as $tag)
                        <option value="{{ $tag->id }}" selected>{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>


        <div class="form-group">
            <div class="col-lg-12">

            </div>
            <div class="col-lg-12">
                <div id="test_image_show">
                    <div class="hasImage {{ !$test->hasImage() ? 'hidden' : '' }}">
                        <div class="wrapper">
                            <button class="btn btn-danger" id="removeTestImage" data-token="{!! csrf_token() !!}" data-url="/test/{{ $test->id }}/image">Remove</button>
                            <img src="{{ asset('images/tests_covers/' . ($test->hasImage() ? $test->test_img_url : 'holder')) }}" alt="">
                        </div>
                    </div>
                    <div class="noImage {{ $test->hasImage() ? 'hidden' : '' }}">
                        <div class="wrapper">
                            <label for="test_image" class="control-label">
                                <span class="glyphicon glyphicon-picture" aria-hidden="true"></span>
                            </label>
                            <input type="file" id="test_image" class="form-control" name="test_image" data-url="/test/{{ $test->id }}/image" multiple>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-12">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </fieldset>
</form>

<br>
@if(!$test->isActive())
<div class="alert alert-danger">
    <strong>This test is not active yet!</strong>
    <p>
        {{ $test->user_id == Auth::user()->id ? 'If you want to activate you test you should add questions to it.' : 'This test is not activated yet, please come again later.' }}
    </p>
</div>
@endif
<div class="clearfix">
    @if($test->active)
    <div>
        <a href="/test/{{ $test->id }}/testing" class="btn btn-info pull-left">Start the test?</a>
    </div>
    @endif
    <div>
        <a href="/test/{{ $test->id }}" class="btn btn-success pull-right">Preview the test?</a>
    </div>
</div>
<div class="panel panel-danger question-delete-section">
    <div class="panel-heading">
        <h3 class="panel-title">Danger zone</h3>
    </div>
    <div class="panel-body">
        <div class="col-md-10">
            <p>Delete question:</p>
            <p class="text-danger">If you delete test all data associated with it will be lost!</p>
        </div>
        <div class="col-md-2">
            <form id="deleteQuestionForm" action="/test/{{ $test->id }}" method="POST">
                <input type="hidden" name="_method" value="DELETE">
                {!! csrf_field() !!}
                <input type="submit" class="btn btn-danger" value="Delete this test">
            </form>
        </div>
    </div>
</div>