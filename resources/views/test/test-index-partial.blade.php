@foreach($tests as $test)
    <div class="col-sm-4 masonry-item">
        <div class="panel panel-info index-test-item">
            <div class="panel-heading">
                <h3 class="panel-title">
                    <a href="/test/{{ $test->id  }}">{{ $test->test_name  }}</a href="/test/{{ $test->id  }}">
                </h3>
            </div>
            <div class="panel-body">
                <p>{{ mb_substr($test->test_short_description, 0, 215) }}</p>
            </div>
        </div>
    </div>
@endforeach