@extends('shared.layout')

@section('content')
    <div class="well bs-component">
        @include('test.partials.creation-loading', ['stage'=>3])
        <div class="row">
            <div class="col-md-12 text-center">
                @include('test.partials.add-question-button')
                <form class="form-horizontal" method="POST" action="/test/test-questions/{{ $test->id }}">
                    <div class="form-group">
                        <div class="col-lg-10 col-lg-offset-2">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                    {!! csrf_field() !!}
                </form>
            </div>
        </div>
    </div>
@endsection