@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix" id="masonry-grid">
        @if($test->isUserOwner())
            <div class="clearfix">
                <a href="/test/{{ $test->id }}/edit" class="btn btn-warning pull-right">Edit the test?</a>
            </div>
            <hr>
        @endif

        <h2>{{ $test->test_name }}</h2>
        <p>{!! $test->test_description !!}</p>

        @if($test->hasImage())
            <div class="col-md-12">
                <img class="img-responsive" src="{{ asset('images/tests_covers/' . $test->test_img_url) }}" alt="">
            </div>
        @endif

        <div class="clearfix">
            <div class="col-md-3">
                <p>
                    Test questions:
                    {{ count($test->questions()) }}
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    Displayed questions:
                    {{ $test->test_displayed_questions == -1 ? "All" : $test->test_displayed_questions }}
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    0
                    times this test have been done.
                </p>
            </div>
            <div class="col-md-3">
                <p>
                    Dificulty:
                    Not rated yet.
                </p>
            </div>
        </div>

        @if(!$test->isActive())
            <div class="alert alert-danger">
                <strong>This test is not active yet!</strong>
                <p>
                    {{ $test->user_id == Auth::user()->id ? 'If you want to activate you test you should add questions to it.' : 'This test is not activated yet, please come again later.' }}
                </p>
            </div>
        @endif

        <br>
        @if($test->isActive())
            <div>
                <a href="/test/{{ $test->id }}/testing" class="btn btn-info pull-left">Start the test?</a>
            </div>
            <div>
                <a href="/file/{{ $test->id }}" class="btn btn-success pull-left">Print the test?</a>
            </div>
        @endif
            
    </div>
@endsection