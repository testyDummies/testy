@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix">
        <ul id="testEditTabNav" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#test" aria-controls="test" role="tab" data-toggle="tab">Test</a></li>
            <li role="presentation"><a href="#questions" aria-controls="questions" role="tab" data-toggle="tab">Questions</a></li>
            <li role="presentation"><a href="#accessibility" aria-controls="accessibility" role="tab" data-toggle="tab">Accessibility</a></li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="test">
                @include('test.partials.test-edit', $test)
            </div>
            <div role="tabpanel" class="tab-pane" id="questions">
                @include('test.partials.questions-edit', $questions)
            </div>
            <div role="tabpanel" class="tab-pane" id="accessibility">Ascscs</div>
        </div>
    </div>
    {{--@include('shared.modals.test.edit-question-modal')--}}
    {{--@include('shared.modals.test.delete-question-modal')--}}
@endsection