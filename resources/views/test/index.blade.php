@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix" id="masonry-grid">
        <div>
            <div class="col-md-12">
                <div id="sidebar-wrapper">
                    <form class="form-vertical">
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-control">
                                    <label for="popular" class="control-label">Popular tests</label>
                                    <input type="radio" name="sort" value="popular" id="popular" class="sort">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-control">
                                    <label for="newest" class="control-label">Biggest tests</label>
                                    <input type="radio" name="sort" value="biggest" id="newest" class="sort">
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-control">
                                    <label for="hardest" class="control-label">Hardest tests</label>
                                    <input type="radio" name="sort" value="hardest" id="hardest" class="sort">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <br>
            </div>
        </div>
        <div class="tests clearfix">
            @include('test.test-index-partial', $tests)
        </div>
    </div>
@endsection