@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix">
        <div class="col-sm-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <h4>Personal data</h4>
                    </h3>
                </div>
                @if(!$isLogged)
                    <div class="panel-body">
                        <div class="list-group">
                            <div class="list-group-item">
                                <span>Name:</span>
                                <span href="#" class="text-center" data-toggle="modal" data-target="#modalName">
                                    {{ $user->name }}
                                </span>
                            </div>

                            <div class="list-group-item">
                                <span>Email:</span>
                                <span href="#" class="text-center" data-toggle="modal" data-target="#modalEmail">
                                    {{ $user->email }}
                                </span>
                            </div>

                            <div class="list-group-item">
                                <span>Username:</span>
                                <span href="#" class="text-center" data-toggle="modal" data-target="#modalUsername">
                                    {{ $user->username ?: 'Not created yet' }}
                                </span>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="panel-body">
                        <div class="list-group">
                            <div class="list-group-item">
                                <span>Name:</span>
                                <a href="#" class="text-center" onclick="updateModal('{{ url('profile/modal') }}', {modal:'name'})" data-target="#modalName">
                                    {{ Auth::user()->name }}
                                </a>
                            </div>

                            <div class="list-group-item">
                                <span>Email:</span>
                                <a href="#" class="text-center" onclick="updateModal('{{ url('profile/modal') }}', {modal:'email'})" data-target="#modalEmail">
                                    {{ Auth::user()->email }}
                                </a>
                            </div>

                            <div class="list-group-item">
                                <span>Username:</span>
                                <a href="#" class="text-center" onclick="updateModal('{{ url('profile/modal') }}', {modal:'username'})" data-target="#modalUsername">
                                    {{ Auth::user()->username ?: 'Create username' }}
                                </a>
                            </div>

                            <div class="list-group-item">
                                <span>Password:</span>
                                <a href="#" class="text-center" onclick="updateModal('{{ url('profile/modal') }}', {modal:'password'})" data-target="#modalPassword">
                                    Change password
                                </a>
                            </div>
                            <div class="list-group-item">
                                <span>Average result: {{ $results->avg('result_percent') == null ? 'Not rated yet' : $results->avg('result_percent') . '/100%' }}.</span>
                            </div>
                        </div>
                    </div>  
                @endif
                    
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <h4>Own tests</h4>
                    </h3>
                </div>
                <div class="panel-body">
                    @if(count($userTests))
                        <div class="list-group">
                            @foreach($userTests as $test)
                                <a href="/test/{{ $test->id }}" class="list-group-item">
                                    {{ $test->test_name }}

                                    @if(!$test->isActive())
                                        <span class="label label-danger">Inactive</span>
                                    @endif
                                </a>
                            @endforeach
                        </div>
                    @else
                        <p>There is no test here...</p>
                        <a href="/test/create">Create a test?</a>
                    @endif
                </div>
            </div>
        </div>

        <div class="col-sm-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <h4>Results</h4>
                    </h3>
                </div>
                <div class="panel-body">
                    @if(count($results))
                        <div class="list-group">
                            @foreach($results as $result)
                                <a href="/result/{{ $result->id }}" class="list-group-item">
                                    {{ $result->test->test_name }}
                                    <span class="pull-right">{{ $result->result_percent }}% <sub>({{ $result->result_points }}/{{ $result->test->displayed_questions() }})</sub></span>
                                </a>
                            @endforeach
                        </div>
                    @else
                        <p>There is no tests done yet...</p>
                        <a href="/test">Make a test?</a>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" role="dialog" id="commonModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection

@section('footer')
    {{--<script src=""></script>--}}
@endsection
