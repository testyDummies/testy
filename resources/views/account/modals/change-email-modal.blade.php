
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Change your email</h4>
</div>
<form role="form" method="POST" action="/profile/changeEmail">
    <input style="display:none;" type="text" name="somefakename" />
    <input style="display:none;" type="password" name="anotherfakename" />
    <div class="modal-body clearfix">
        <div class="col-md-12">
            <div class="form-group">
                <label for="email">Enter Email</label>
                <div class="input-group">
                    <input type="email" class="form-control" name="email" id="email" placeholder="Enter you new email" required="" value="{{ old('newEmail') ?: Auth::user()->email }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password_mail">Enter Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" id="password_mail" name="password_name" placeholder="Confirm Email" required="">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
    {!! csrf_field() !!}
</form>

