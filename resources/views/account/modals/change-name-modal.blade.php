
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Change your name</h4>
</div>
<form role="form" method="POST" action="/profile/changeName" data-validate="true" id="changeNameForm">
    <div class="modal-body clearfix">
        <div class="col-md-12">
            <div class="alert alert-dismissible hidden">
                <button type="button" class="close" data-dismiss="alert">?</button>
                <strong></strong>
            </div>
            <div class="form-group">
                <label for="newName">Enter Name</label>
                <div class="input-group">
                    <input type="text" class="form-control" name="newName" id="newName" placeholder="Enter Name" required="" value="{{ old('name') ?:  Auth::user()->name }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password_name">Enter Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" id="password" name="password_name" placeholder="Confirm Your Password" required="">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary"><span id="loading" style="display: none" class="glyphicon glyphicon-refresh glyphicon-refresh-animate center-text"></span> Save changes</button>
    </div>
    {!! csrf_field() !!}
</form>
