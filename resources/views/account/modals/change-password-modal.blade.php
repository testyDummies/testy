
<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h4 class="modal-title" id="myModalLabel">Change your name</h4>
</div>
<form role="form" method="POST" action="/profile/changePassword">
    <div class="modal-body">
        <div class="col-md-12">
            <div class="form-group">
                <label for="old_password">Enter old Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" name="password_name" id="old_password" placeholder="Enter you old password" required="" value="">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password_new">Enter new Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" name="password_new" id="password_new" placeholder="Enter Name" required="" value="">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password_new_confirmation">Repeat new Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" id="password_new_confirmation" name="password_new_confirmation" placeholder="Confirm Email" required="">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>
            {{--<input type="submit" name="submit" id="submit" value="Sign up" class="btn btn-info pull-right">--}}
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
    {!! csrf_field() !!}
</form>