@extends('shared.layout')

@section('content')
	<div id="timer"></div>
    <div class="well bs-component clearfix">
        <div class="row">
        	<div class="col-md-8 col-md-offset-2">
        		<form id="testingForm" action="/test/{{ $test->id }}/testing" method="post">
					@foreach($test->questions as $index => $question)
						@include('question.' . getClassName($question->question_reference->question_type->table_name) . '.testing-partial')
	        		@endforeach
					<div class="">
						<button class="btn btn-info pull-right" type="submit">Finish test</button>
					</div>
					{!! csrf_field() !!}
        		</form>
        	</div>
        </div> 
    </div>
@endsection