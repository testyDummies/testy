@foreach($questions as $index => $question)
    <div class="panel panel-success row">
        @include('question.' . (new ReflectionClass($question))->getShortName() . '.print-partial', [$index])
    </div>
@endforeach