@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix">
        <div class="row">
            <div class="col-md-4">
                <span class="name">{{ $test->test_name }}</span>
            </div>
            <div class="col-md-4">
                <span class="result">
                    {{ $result->result_points }}/{{ $test->displayed_questions() }}
                </span>
            </div>
            <div class="col-md-12">
                <ol>
                    @foreach($sequence as $question)
                        <li>
                            {{ $question['q_c'] }}
                            <ul>
                                @foreach($question['answers'] as $answer)
                                    <li>
                                        <p {!! $answer['a_t'] != $answer['u_t'] ? 'class="text-danger"' : 'class="text-info"'!!}}>
                                            {{ $answer['a_c'] }}
                                        </p>
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    @endforeach
                </ol>
            </div>
        </div>
    </div>
@stop