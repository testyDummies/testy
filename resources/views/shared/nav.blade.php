<div class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a href="{{ url('/')  }}" class="navbar-brand">Testy</a>
            <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse" id="navbar-main">
            <ul class="nav navbar-nav">
                <li>
                    <a href="/">Home</a>
                </li>
                <li>
                    <a href="/test">Browse tests</a>
                </li>
                @if(Auth::check())
                    <li>
                        <a href="/test/create">Create test</a>
                    </li>
                    <li>
                        <a href="/group">Groups</a>
                    </li>
                @endif
            </ul>

            <ul class="nav navbar-nav navbar-right">
                @if(!Auth::check())
                    <li>
                        <a href="/login">Sign in</a>
                    </li>
                    <li>
                        <a href="/register">Sign up</a>
                    </li>
                @else
                    <li>
                        <a href="/invite">Invites <span class="badge">{{ $invites_count }}</span></a>
                    </li>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" id="navFullName">{{ Auth::user()->name }}<span class="caret"></span></a>
                        <ul class="dropdown-menu" aria-labelledby="download">
                            <li>
                                <a href="/user">Profile</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="/logout">Sign out</a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>

        </div>
    </div>
</div>