<div class="modal" id="editQuestionModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">?</button>
                <h4 class="modal-title">Are you sure you want to delete this question?</h4>
            </div>
            <div class="modal-body">
                <p>Instead of deleteting questions you can just exclude them from list of displayed questions.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger">Delete selected answer</button>
            </div>
        </div>
    </div>
</div>