<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Testy</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/1.0.1/vue-resource.min.js"></script>

</head>
<body>
    @include('shared.nav')

    <div class="container">
        @include('shared.errors')
        @yield('content')
    </div>
    <footer class="footer">
        <hr>
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3 class="text-center">Testy {{ date("Y") }}&copy; Alpha 0.1</h3>
                </div>
            </div>
        </div>
        <hr>
    </footer>
    <script src="{{ asset('js/app.js')  }}" type="text/javascript"></script>
    @yield('footer')
</body>
</html>