
<h3>{{ $index + 1 }}. {{ $question->content }}</h3>
<input type="hidden" name="question[{{ $question->id }}]" value="{{ $question->id }}">
<textarea style="width: 100%;" rows="10"></textarea>
@if($question->img_url)
    <div class="col-md-12">
        <img class="img-responsive" src="{{ asset('images/questions/' . $question->img_url) }}" alt="">
    </div>
@endif
