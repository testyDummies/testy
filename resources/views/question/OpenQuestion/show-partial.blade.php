<div class="list-group question">
    <div class="list-group-item active clearfix">
        <a href="/question/{{ $question->id }}/edit" class="btn btn-warning pull-right">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>

        <button class="delete-answer-btn btn btn-danger pull-right">
            <span class="glyphicon glyphicon-trash"></span>
        </button>
        <p>{{ $question->content }}</p>
    </div>
    @foreach($question->answers as $answer)
    <span class="list-group-item popover-link">
            {{   $answer->content }}
    </span>
    @endforeach
</div>