<form role="form" id="addOpenQuestion" action="/open-question" method="POST" enctype="multipart/form-data"
      xmlns:v-on="http://www.w3.org/1999/xhtml">
    <fieldset>
        <legend>New Open Question</legend>
        <div class="form-group">
            <label for="testName" class="col-lg-12 control-label">Question:</label>
            <div class="col-lg-12">
                <textarea required class="form-control" rows="3" name="content" id="questionContent" placeholder="Question to ask."></textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-lg-12">
                <label class="control-label">Settings:</label>
            </div>
            <div class="col-lg-4">
                <label for="active">Question active:</label>
                <input type="checkbox" id="active" name="active" checked>
            </div>
            <div class="col-lg-4">
                <label for="active">Exact match:</label>
                <input type="checkbox" id="exact" name="exact" v-model="exact">
            </div>
            <div class="col-lg-4">
                <label for="img">Image:</label>
                <input type="file" id="img" class="form-control" name="img">

                <br><br>
            </div>
        </div>
        <div class="form-group" v-if="exact">
            <label class="col-lg-2 control-label">Answers</label>
            <div class="col-lg-10">

                <div class="answersInput">
                    <div v-for="item in items" class="answer-group">
                        <div class="input-group">
                            <textarea required class="form-control" rows="2" name="answer[@{{ $index + 1 }}]" placeholder="Answer @{{ $index + 1 }}"></textarea>
                            <span v-if="items.length > 1" class="input-group-addon">
                                <button v-on:click="removeAnswerInput($index)" class="delete-answer-btn btn btn-danger pull-right"><span class="glyphicon glyphicon-trash">
                                    </span>
                                </button>
                            </span>
                        </div>
                        <div class="col-lg-10 col-lg-offset-2">
                            <textarea class="form-control" rows="2" name="answer_explanation[@{{ $index + 1 }}]" placeholder="Explanation for question @{{ $index + 1 }}"></textarea>
                        </div>
                    </div>
                </div>

                <span class="help-block">There can be 2 or more answers.</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-lg-10 col-lg-offset-2">
                <button type="button" id="addAnswer" v-if="exact" v-on:click="addAnswerInput" class="btn btn-success">
                    Add answer
                </button>
                <button type="submit" class="btn btn-primary pull-right">Submit</button>
            </div>
        </div>
        <input type="hidden" name="test_id" value="{{ $testId }}">
        <input type="hidden" name="type_id" value="{{ $questionType->id }}">
        {!! csrf_field() !!}
    </fieldset>
</form>

<script>
    new Vue({
        el: 'body',
        data: {
            exact: false,
            items: [
                {  }
            ]
        },
        methods: {
            addAnswerInput: function() {
                this.items.push({
                    message: ""
                });
            },
            removeAnswerInput: function(index) {
                this.items.splice(index, 1);
            }
        }
    });
</script><?php
/**
 * Created by PhpStorm.
 * User: vi4ov
 * Date: 18.2.2017 г.
 * Time: 14:53
 */