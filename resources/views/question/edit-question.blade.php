<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
    <h4>Edit Question</h4>
</div>
<div class="modal-body">
    <form role="form" id="addQuestion" action="/question/{{ $question->id    }}" method="POST">
        <input name="_method" type="hidden" value="PUT">
        <fieldset>
            <div class="form-group">
                <label for="testName" class="col-lg-2 control-label">Question:</label>
                <div class="col-lg-10">
                    <textarea class="form-control" rows="3" name="question_content" id="questionContent" placeholder="Question to ask.">{{ $question->question_content  }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-lg-2 control-label">Answers</label>
                <div class="col-lg-10">
                    <div class="answersInput">
                        @foreach($question->answers as $k => $answer)
                            <input type="hidden" name="answer_id[{{ $counter }}]" value="{{ $answer->id }}">
                            <div class="answer-group">
                                <div class="input-group">
                                    <span class="input-group-addon"><input type="checkbox" name="right[{{ $counter }}]" {{ $answer->answer_right ? "checked" : "" }}></span>
                                    <textarea required class="form-control" rows="2" name="answer[{{ $counter }}]" placeholder="Answer {{ $counter }}">{{ $answer->answer_content }}</textarea>
                                    @if($k > 2)
                                        <span class="input-group-addon">
                                            <a href="#" class="delete-answer-btn btn btn-danger pull-right">
                                                <span class="glyphicon glyphicon-trash"> </span>
                                            </a>
                                        </span>
                                    @endif
                                </div>
                                <div class="col-lg-10 col-lg-offset-2">
                                    <textarea class="form-control" rows="2" name="answer_explanation[{{ $counter }}]" placeholder="Explanation for question {{ $counter++ }}">{{ $answer->answer_explanation }}</textarea>
                                </div>

                            </div>
                        @endforeach
                    </div>

                    <span class="help-block">There can be 2 or more answers.</span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-10 col-lg-offset-2">
                    <a href="#" class="btn btn-success addAnswer">
                        Add answer
                    </a>
                    <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
            </div>
            {!! csrf_field() !!}
        </fieldset>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <button type="button" class="btn btn-danger">Delete selected answer</button>
</div>