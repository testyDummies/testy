<div class="list-group question">
    <div class="list-group-item active clearfix">
        <p>{{ $question->content }}</p>
        <a href="/question/{{ $question->id }}/edit" class="btn btn-warning pull-right">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>

        <button class="delete-answer-btn btn btn-danger pull-right">
            <span class="glyphicon glyphicon-trash"></span>
        </button>

    </div>
    @foreach($question->answers as $answer)
        <span class="list-group-item popover-link">
            <span class="glyphicon {{ $answer->right ? 'glyphicon-ok' : 'glyphicon-remove' }}" title=""></span>
            {{   $answer->content }}
        </span>
    @endforeach
</div>