<h3>{{ $index + 1 }}. {{ $question->content }}</h3>
<input type="hidden" name="question[{{ $question->id }}]" value="{{ $question->id }}">
@foreach($question->answers as $answer)
    <div class="answer">
        <label>
            <input class="switcher"	type="checkbox"
                      name="answer[{{ $question->id }}][{{ $answer->id }}]"
                      value="{{ $answer->id }}">
            <span> {{ $debug ? $answer->right : "" }} {{ $answer->content }}</span>
        </label>
    </div>
@endforeach

@if(hasImage($question->img_url))
    <div class="col-md-12">
        <img class="img-responsive" src="{{ asset('images/questions/' . $question->img_url) }}" alt="">
    </div>
@endif