
<h3>{{ $index + 1 }}. {{ $question->content }}</h3>
<input type="hidden" name="question[{{ $question->id }}]" value="{{ $question->id }}">
@if($question->img_url)
    <div class="col-md-12">
        <img class="img-responsive" src="{{ asset('images/questions/' . $question->img_url) }}" alt="">
    </div>
@endif
@foreach($question->answers as $answer)
    <div class="answer">
        <label>
            <input 	type="checkbox"
                      name="answer[{{ $question->id }}][{{ $answer->id }}]"
                      value="{{ $answer->id }}">
            <span>{{ $answer->content }}</span>
        </label>
    </div>

@endforeach
