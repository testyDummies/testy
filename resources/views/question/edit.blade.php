@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix">
        <a href="/test/{{ $question->test_id }}/edit" class="btn btn-success">Back to test edit</a>
        <form method="post" class="delete-question-form" action="/question/{{ $question->id }}"  enctype="multipart/form-data">
            <input type="hidden" name="_method" value="DELETE">
            <button type="submit" class="btn btn-danger">
                <span class="glyphicon glyphicon-trash"></span>
            </button>
            {!! csrf_field() !!}
        </form>
        <form role="form" id="addQuestion" action="/question/{{ $question->id }}" method="POST" enctype="multipart/form-data">
            {!! method_field('PUT') !!}
            {!! csrf_field() !!}
            <fieldset>
                <legend>Edit Question</legend>
                <div class="form-group">
                    <label for="testName" class="col-lg-12 control-label">Question:</label>
                    <div class="col-lg-12">
                        <textarea class="form-control" rows="3" name="question_content" id="questionContent" placeholder="Question to ask.">{{ $question->question_content }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-12">
                        <label class="control-label">Settings:</label>
                    </div>
                    <div class="col-lg-4">
                        <label for="question_active">Question active:</label>
                        <input type="checkbox" id="question_active" name="question_active" {{ $question->question_active == 0 ? "" : "checked" }}>
                    </div>
                    <div class="col-lg-4">
                        <label for="question_answers_visible">Answers visible:</label>
                        <input type="number" min="2" id="question_answers_visible" value="{{ $question->question_answer_visible }}" class="form-control" name="question_answer_visible">
                    </div>
                    <div class="col-lg-4">
                        <label for="question_img">Image:</label>
                        <input type="file" id="question_img" class="form-control" name="question_img">

                        <br><br>
                    </div>
                    @if($question->question_img_url != 'none')
                        <div class="col-md-12">
                            <img class="img-responsive" src="{{ asset('images/questions/' . $question->question_img_url) }}" alt="">
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label class="col-lg-12 control-label">Answers</label>
                    <div class="col-lg-12">

                        <div class="answersInput">
                            @foreach($question->answers as $k => $answer)
                                <div class="answer-group">
                                    <div class="input-group">
                                        <span class="input-group-addon"><input type="checkbox" name="right[1]" {{ $answer->answer_right ? "checked" : "" }}></span>
                                        <textarea required class="form-control" rows="2" name="answer[1]" placeholder="Answer {{ $k + 1 }}">{{ $answer->answer_content }}</textarea>
                                        <span class="input-group-addon">
                                            @if(count($question->answers) > 2)
                                                <form onsubmit="return ConfirmDelete()" method="post" action="/answer/{{ $answer->id }}">
                                                    {{--class="delete-question-form"--}}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <button type="submit" class="btn btn-danger">
                                                        <span class="glyphicon glyphicon-trash"></span>
                                                    </button>
                                                    {!! csrf_field() !!}
                                                </form>
                                            @endif
                                        </span>
                                    </div>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-info-sign"></span>
                                        </span>
                                        <textarea class="form-control" rows="2" name="answer_explanation[1]" placeholder="Explanation for question {{ $k + 1 }}">{{ $answer->answer_explanation }}</textarea>
                                    </div>
                                    <hr>
                                </div>
                            @endforeach
                        </div>

                        <span class="help-block">There can be 2 or more answers.</span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <a href="#" id="addAnswer" class="btn btn-success">
                            Add answer
                        </a>
                        <button type="submit" class="btn btn-primary pull-right">Submit</button>
                    </div>
                </div>
                <input type="hidden" name="test_id" value="{{ $question->test_id }}">
            </fieldset>
        </form>
    </div>
@endsection