<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
    <h4 class="modal-title">Add question</h4> </div>
<div class="modal-body">
    <div class="row">
        <div class="col-md-3">
            <div class="list-group table-of-contents">
                @foreach($questionTypes as $questionType)
                        <a class="list-group-item" onclick="loadQuestionForm('{{ url('/question-type/' . $questionType->id) }}',{test_id : {!! $testId !!}})">{{ $questionType->display_name }}</a>
                @endforeach
            </div>
        </div>
        <div class="col-md-9 question-content"></div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
</div>