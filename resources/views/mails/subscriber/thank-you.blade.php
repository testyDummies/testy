<br>
Hello {{ $sub->name }},<br>
<br>
Testy is currently in heavy development. <br>
Thank you for your interest, we will be in touch when everything is ready.<br>
<br>
Regards,<br>
Testy team<br>
<br>
---<br>
<br>
Здравейте {{ $sub->name }},<br>
<br>
Testy в момента е в интензияна разработка. <br>
Благодарим ви за интереса, ще се свържем с вас когато всичко е готово.<br>
<br>
С уважение,<br>
Екипа на Testy<br>
