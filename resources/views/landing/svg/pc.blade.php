<svg version="1.1" class="pc" xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" width="1298px" height="751px" viewBox="0 0 1298 751" enable-background="new 0 0 1298 751" xml:space="preserve">
	<path fill-rule="evenodd" clip-rule="evenodd" d="M143.667,696V0h1010.999v696H143.667z M165.667,670h966.999V30H169.452 l-0.213,0.031c-3.823,0.551-3.659,4.594-3.572,6.06V670z"/>
	<rect y="709" fill-rule="evenodd" clip-rule="evenodd" width="1298.334" height="42"/>
	<circle fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" cx="649.167" cy="16.61" r="8.083"/>
</svg>