@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix">
        <div class="div">
            <div class="col-md-12">
                <a href="/group/create" class="btn btn-info">Create group</a>
            </div>
        </div>
        <div class="tests clearfix">
            @foreach($groups as $group)
                <div class="col-sm-4 masonry-item">
                    <div class="panel panel-info index-test-item">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                <a href="/group/{{ $group->id  }}">{{ $group->group_name  }}</a>
                            </h3>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection