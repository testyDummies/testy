@extends('shared.layout')

@section('content')
    <div class="well bs-component">
        <form class="form-horizontal" action="/group" method="POST">
            <fieldset>
                <legend>Create Test</legend>
                <div class="form-group">
                    <label for="groupName" class="col-lg-2 control-label">Name of the group</label>
                    <div class="col-lg-10">
                        <input type="text" class="form-control" id="groupName" name="group_name" placeholder="Name of the group" required ng-minlength="8" ng-maxlength="100">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-lg-2 control-label">Description</label>
                    <div class="col-lg-10">
                        <textarea class="form-control" rows="3" name="group_description" id="groupDescription"  required ng-minlength="8"></textarea>
                        <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="groupType" class="col-lg-2 control-label">Choose group type:</label>

                    <div class="col-lg-10"  ng-init="group.getGroupTypes()">
                        <div class="clearfix">
                            <input type="checkbox" name="listable" id="listable">
                            <label for="listable">Listble</label>
                            <br>
                            <small>Is the group will be listed in group browser.</small>
                        </div>
                        <div class="clearfix">
                            <input type="checkbox" name="invitable" id="invitable">
                            <label for="invitable">Invitable</label>
                            <br>
                            <small>Will users be invited to the group or they will be freely join.</small>
                        </div>
                        <div class="clearfix">
                            <input type="checkbox" name="publicContent" id="publicContent">
                            <label for="publicContent">Public content</label>
                            <br>
                            <small>Will all the content going to be with public access.</small>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-lg-10 col-lg-offset-2">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </fieldset>

            {!! csrf_field() !!}
        </form>
    </div>
@endsection