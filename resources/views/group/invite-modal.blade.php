<!-- Invite User Modal -->
<div class="modal fade" id="inviteUser" tabindex="-1" role="dialog" aria-labelledby="inviteUser">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Invite users to this group</h4>
            </div>
            <div class="modal-body clearfix">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label for="user" class="control-label">Type user's name or user's username:</label>
                        <input class="form-control" type="text" id="userGroupSearch" name="user" placeholder="Start typing...">
                    </div>
                </form>
                <ul id="userlist">

                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">Close</button>
            </div>
        </div>
    </div>
</div>