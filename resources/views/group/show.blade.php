@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix" id="masonry-grid">
        <h2>{{ $group->group_name }}</h2>

        <a href="/group/{{ $group->id }}/join" class="btn btn-info"  data-toggle="modal" data-target="#inviteUser">Invite people to group</a>

        @if($group_type['invitable'] && !Auth::user()->inGroup($group->id))
            <a href="/group/{{ $group->id }}/user/join" class="btn btn-success">Join group</a>
        @endif

        @if($group_type['publicContent'])
            Some content
        @endif
    </div>
    <div id="group" data-group-id="{{ $group->id }}"></div>
@endsection

@include('group.invite-modal')