@extends('shared.layout')

@section('content')

    <div class="row">
        <form role="form" method="POST" action="/login">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="InputEmail">Enter Email</label>
                    <div class="input-group">
                        <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required="">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="InputEmail">Enter Password</label>
                    <div class="input-group">
                        <input type="password" class="form-control" id="password" name="password" placeholder="Confirm Email" required="">
                        <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                    </div>
                </div>
                <div>
                    <input type="checkbox" name="remember"> Remember Me
                </div>
                <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-info pull-right">
                <a href="/password/email" class="btn btn-warning pull-right">Forgot password?</a  >
            </div>
            {!! csrf_field() !!}
        </form>
    </div>
@endsection