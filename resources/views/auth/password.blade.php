@extends('shared.layout')

@section('content')

    <div class="row">
        <form role="form" method="POST" action="/password/email">
            {!! csrf_field() !!}
            <div class="form-group">
                <label for="InputEmail">Enter Email</label>
                <div class="input-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required="" value="{{ old('email') }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <input type="submit" name="submit" id="submit" value="Send Password Reset Link" class="btn btn-info pull-right">
        </form>
    </div>

@endsection