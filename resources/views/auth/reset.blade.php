@extends('shared.layout')

@section('content')

    <div class="row">
        <form role="form" method="POST" action="/password/reset">
            {!! csrf_field() !!}
            <input type="hidden" name="token" value="{{ $token }}">

            <div class="form-group">
                <label for="InputEmail">Enter Email</label>
                <div class="input-group">
                    <input type="email" class="form-control" id="email" name="email" placeholder="Enter Email" required="" value="{{ old('email') }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" id="password" name="password" placeholder="Enter New Password" required="" value="{{ old('password') }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <div class="form-group">
                <label for="password_confirmation">Confirm Password</label>
                <div class="input-group">
                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="Enter Password confirmation" required="" value="{{ old('password_confirmation') }}">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-asterisk"></span></span>
                </div>
            </div>

            <input type="submit" name="submit" id="submit" value="Reset Password" class="btn btn-info pull-right">
        </form>
    </div>

@endsection