@extends('shared.layout')

@section('content')

    <div class="well">
        <div class="jumbotron">
            <h1>Welcome to Testy!</h1>
            <h3>Free online testing service.</h3>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center">Last 10 tests.</h3></div>
                    <div class="panel-body">
                        <div class="list-group">
                            @foreach($lastTests as $lastTest)
                                <a href="/test/{{ $lastTest->id }}" class="list-group-item">{{ $lastTest->test_name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center">Top 10 tests.</h3></div>
                    <div class="panel-body">
                        <div class="list-group">
                            @foreach($topTests as $topTest)
                                <a href="/test/{{ $topTest->id }}" class="list-group-item">{{ $topTest->test_name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3 class="text-center">Top 10 users.</h3></div>
                    <div class="panel-body">
                        <div class="list-group">
                            @foreach($topUsers as $topUser)
                                <a href="/user/{{ $topUser->username }}" class="list-group-item">{{ $topUser->name }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection