@extends('shared.layout')

@section('content')
    <div class="well bs-component clearfix" id="masonry-grid">
        <div class="row">
            <div class="col-md-12">
                <h1>Invites</h1>
                <ul class="list-group">
                    @if(count($invites) > 0)
                        @foreach($invites as $invite)
                            <li class="list-group-item clearfix">
                                <a href="/group/{{ $invite->group->id }}">{{ $invite->group->group_name }}</a>
                                from {{ $invite->fromUser->name }} |
                                {{ $invite->created_at->toFormattedDateString() }}

                                <div class="pull-right">
                                    <form action="/invite/{{ $invite->id }}" method="POST">
                                        <button class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
                                        {!! method_field('DELETE') !!}
                                        {!! csrf_field() !!}
                                    </form>
                                </div>
                            </li>
                        @endforeach
                    @else
                        <li class="list-group-item">There are currently no pending invites.</li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
@endsection